a = []

# Agregar elementos
a.push("soy un string")
a.push("yo tambien", true, 4)
puts "#{a}"

a.insert 0, "Lunes", "Martes"
puts "#{a}"

# Eliminar elementos
a.pop
puts "#{a}"

a.pop(2)
puts "#{a}"

# Eliminar elementos especificos por indice
a = (1..8).to_a
a.delete_at(2)
puts "#{a}"

# Eliminar elementos especificos por elemento
a = (1..8).to_a
a.delete(5)
puts "#{a}"

a = ("cara".."card").to_a
puts "#{a}"
a.delete("carb")
puts "#{a}"

a = (1..20).to_a

# Desordenar
puts "Desordenado: #{a.shuffle}"
# Ordenar
puts "Ordenado: #{a.sort}"
# Ordenar mayor-menor // voltear
puts "Reordenado: #{a.reverse}"
# Sacar uno aleatorio
puts "Ejemplo: #{a.sample}"




