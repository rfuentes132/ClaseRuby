# Generar una matriz 15x15 de numeros enteros y aleatorios e intercambiar sus filas por sus columnas

matriz = []
aux = 0

puts "Generando arreglo base"
puts

lim = 14

for i in 0..lim do
	matriz[i] = []
	for j in 0..lim do
		matriz[i][j] = rand(100)
		print " #{matriz[i][j]} "
		
		if ( matriz[i][j].to_s.length < 2 )
			print " "
		end
		print "|"
	end
	puts
end

puts
puts "Arreglo intercambiado"
puts

for i in 0..14 do
	for j in 0..14 do
		aux = matriz[i][j]
		matriz[i][j] = matriz[j][i]
		matriz[j][i] = aux

		print " #{matriz[i][j]} "
		
		if ( matriz[i][j].to_s.length < 2 )
			print " "
		end
		print "|"

	end
	puts
end

