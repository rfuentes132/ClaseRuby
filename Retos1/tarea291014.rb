# Escriba el codigo ruby que genere una matriz 20x20 de numeros aleatorios
# distintos, la ordene de menor a mayor y devuelva la posición (par 
# ordenado) en la matriz ordenada en la que se encuentra un numero dado
# por teclado

# Enviar codigo fuente por PM en SLACK

lim = 20
lim1 = lim -1
rep = false
i = 0
matriz = []

puts "Generando Matriz base"
puts

# Primer ciclo crea cada arreglo dentro del primero e imprime luego de tenerlo lleno
for i in 0..lim1

	matriz[i] = Array.new(lim)
	j = 0

	# While para llenar cada arreglo interno
	while j < matriz[i].length
		
		x = rand(lim**2)
		rep = false

		# Verificacion
		p = 0
		while p <= i
			k = 0		
		
			while ( (k < matriz[p].length) && (rep != true) )

				if ( x == matriz[p][k] )
					rep = true
				end

				k += 1
			end
			p += 1
		end


		# Sino se repite se agrega
		if (rep == false)
			matriz[i][j] = x
			j += 1
		end	
	end

	puts "#{i+1} " + matriz[i].to_s
end

# Clonando matriz
# matrizBase = matriz 		// FU Ruby
matrizBase = []

for i in 0..lim1
	matrizBase[i] = []
	for j in 0..lim1
		matrizBase[i][j] = matriz[i][j]
	end
end

puts
puts "Reorganizando Matriz"
puts

# Bubble sort
# 1:20am (esto fue googleado #GraciasStackOverflow #GrasiaChiabe #GraciasJuan)
for i1 in 0..lim1 do
	for j1 in 0..lim1 do
		for i2 in 0..lim1 do
			for j2 in 0..lim1 do				
				if (matriz[i1][i2] < matriz[j1][j2])
					aux = matriz[i1][i2]
					matriz[i1][i2] = matriz[j1][j2]
					matriz[j1][j2] = aux
				end
			end
		end
	end
end

# Imprimir matriz ordenada
for i in 0..lim1
	puts matriz[i].to_s
end

puts "Introduzca numero a buscar"
puts "Se retornara su posición (par ordenado) en la matriz base (sin ordenar)"
num = gets.chomp.to_i

# Imprimir posición
for i in 0..lim1
	for j in 0..lim1
		if ( num == matrizBase[i][j] )
			puts "Posición (#{i+1},#{j+1})"
		end
	end
end