# Generar un arreglo de 20 numeros aleatorios enteros 
# y devolverlo ordenado de mayor a menor

arr = Array.new(15)
aux = 0


for i in 0..14 do
	arr[i] = rand(101)
end

puts "Arreglo base #{arr}"

# Metodo Burbuja
for i in 0..14 do
	for i in 0..13 do
		if (arr[i] > arr[i+1])
			aux = arr[i]
			arr[i] = arr[i+1]
			arr[i+1] = aux
			# puts "cambiando #{arr[i]} por #{arr[i+1]}"
		end
	end
end

puts "Arreglo ordenado #{arr}"

