# Genere un arreglo de numeros aleatorios de 15 valores
# del 0 al 100 y determine el menor, mayor y promedio

arr = Array.new(15)
max = 0
min = 100
prom = 0

for i in 0..14 do

	arr[i] = rand(100) + 1

	if (arr[i] > max)
		max = arr[i]
	end

	if (arr[i] < min)
		min = arr[i]
	end

	prom = prom + arr[i]
end

prom = prom/15

puts "Arreglo: #{arr}"
puts "Maximo: #{max}"
puts "Minimo: #{min}"
puts "Promedio: #{prom}"
