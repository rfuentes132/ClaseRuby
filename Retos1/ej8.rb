# Mostrar cantidad de numeros pares e impares entre 1 y un numero dado

puts "Introducir numero:"
lim = gets.chomp.to_i

par = 0
impar = 0


for i in 1..lim do
	if (i % 2 == 0 ) 
		par += 1
	else
		impar += 1
	end
end

puts "Pares: #{par}"
puts "Impares: #{impar}"
