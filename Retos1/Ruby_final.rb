# Crear un programa que implemente una version del juego de vagos que permita jugar hasta (6) jugadores,
# llevar el control de turnos de cada jugador, conteo parcial de las puntuaciones y finalmente 
# mostrar los resultados finales e indicar que jugador debe hacer la penitencia.

# Fases
# 1. Pedir al usuario la cantidad de jugadores que participaran
# 2. Indicar a que jugador le corresponde hacer el lanzamiento
# 3. Hacer lanzamiento presionando Enter y observar en que cuadricula cae la moneda
# 4. Repetir el turno hasta consumir las 3 jugadas
# 5. En todo momento mostrar estado actual del juego, jugador de turno, cuadricula del piso y 
# lugar donde cae la moneda (siempre en la cuadricula)

# ------------------------------------------------
# Casos de uso
# Al iniciar el programa, la primera vez entrar directo a settings
# Configurar cantidad de jugadores
# Tamaño del tablero
# Cantidad de turnos
# Luego ir al menu y mostrar Jugar y Configuraciones
# Al comenzar a jugar imprimir cuadricula
# Mostrar a la derecha jugador de turno
# Y debajo puntajes del jugador
# Al finalizar todos los turnos mostrar puntuaciones completas
# Y luego sugerir penitencias




# Solicitar cantidad de jugadores
def players # Retorna cant_players

	valid = false
	max_players = 6
	min_players = 1

	while ( valid != true)

		puts "Introduzca cantidad de jugadores"
		cant_players = gets.chomp.to_i

		if ( cant_players < min_players ) || ( cant_players > max_players)
			puts "Numero invalido. Deben ser entre #{min_players} y #{max_players}"
		else
			valid = true
		end
	end

	return cant_players 
end

# Seleccionar tamaño del tablero
def board_size # Retorna size[x,y]

	valid = false

	size = [0,0]

	while ( valid == false)

		valid = true

		puts "Seleccionar dificultad (tamaño del tablero)"
		puts "1. Normal	<Normal>	(7x9)"		# i = 7 | j = 9
		puts "2. Dificil	<Grande>	(11x7)"	# i = 10| j = 13
		puts "0. Personalizar"
		opt = gets.chomp.to_i

		if opt == 0
			puts "Largo (i): "
			size[0] = gets.chomp.to_i
			puts "Ancho (j): "
			size[1] = gets.chomp.to_i
		elsif opt == 1			
			size = [7,9]
		elsif opt == 2
			size = [11,7]
		else
			puts "Opción incorrecta"
			puts
			valid = false
		end

	end

	return size
	# puts size.to_s
end

# Generar Tablero
def build_board (size) # Retorna board[][]

	# Guardar la mitad de la longitud de j
	# Reiniciar la cuenta en cada iteracion de i
	# Si j es menor q la mitad menos i sumar 1
	# Si j es mayor a la mitad mas i restar 1
	# Si el caso anterior se da y la longitud es par
	# debe ser mayor o igual que.

	board_size_i = size[0] - 1
	board_size_j = size[1] - 1

	half = board_size_j / 2	

	board = []

	for i in 0..board_size_i
		board[i]=[]
		count = 0
		for j in 0..board_size_j
			board[i][j] = count

			if j < (half-i)
				count += 1
			end	

			if (board_size_j%2 == 0)
				if j >= (half+i)
					count -= 1
				end
			else
				if j > (half+i)
					count -= 1
				end
			end
		end
		# Descomentar para imprimir matriz
		# puts board[i].to_s
	end

	return board
end

# Solicitar nombres de usuarios
def users (cant_players) # Retorna playerNames[]

	playerNames = []
	for i in 0..cant_players-1
		puts "Introduzca nombre de jugador #{i+1}"
		playerNames[i] = gets.chomp.to_s
	end

	return playerNames
end

# Solicitar cantidad de turnos
def turnos # Retorna turnos 

	turnos = 0
	
	while turnos < 1
		puts "Introduzca cantidad de turnos por jugador"
		turnos = gets.chomp.to_i
	end

	return turnos
end

# Generar matriz de resultados vacía
def results(cant_players) # Retorna results[][]

	results = []

	for i in 0..cant_players-1
		results[i] = []
	end

	return results
end

# Lanzamiento de moneda
def jugar(board, playerNames, turnos, results)
	
	for p in 0..playerNames.length-1

		punt = 0
		suma = 0
		player = playerNames[p]
		
		for t in 0..turnos-1

			system("clear")
			puts "	Turno #{t+1} de #{player}"
			puts "	Presione enter para lanzar"
			gets

			coin = (rand(2)>0) ? "Cara":"Sello"
			coordi = rand(board.length)
			coordj = rand(board[0].length)

			for i in 0..board.length-1
					print "	"
				for j in 0..board[i].length-1
					print " "
					if (coordi == i) && (coordj == j)
				
						print "#"
						punt = board[coordi][coordj]
						if coin == "Cara"
							punt = punt * 2
						end
				
					else
						print board[i][j]
					end
				end
				
				puts
			end
			
			puts
			# puts "	Jugador: #{player}	Turno: #{t+1}/#{turnos}"
			puts "	Moneda: #{coin}	Puntuacion: #{punt}"				
			puts "	Fila: #{coordi+1}	Columna: #{coordj+1}"
			puts

			results[p][t] = punt

			suma += punt
			gets
		end

		results[p][results[p].length] = suma
		# puts results[p].to_s
	end

	return results
end

# Imprimir resultados
def printResults(playerNames, results, penitencias)
	
	win = 0
	winner = ""
	lose = 1000000000000000000
	loser = ""

	for i in 0..playerNames.length-1

		suma = results[i][results[i].length-1]
		player = playerNames[i]

		print "Jugador #{player} obtuvo "
		
		for j in 0..results[i].length-2
			print " #{results[i][j]} "

		end

		if win < results[i][results[i].length-1]
			win = results[i][results[i].length-1]
			winner = player
		end

		if lose > results[i][results[i].length-1]
			lose = results[i][results[i].length-1]
			loser = player
		end

		puts "por turno."
		puts "Con un total de #{results[i][results[i].length-1]}"
		puts
	end

	if win == lose
		puts "Tenemos un empate!"		
	else
		puts "El ganador es #{winner}! Felicitaciones!!!"
		puts
		puts "#{loser} ha perdido, como penitencia debe:"
		puts penitencias[rand(penitencias.length-1)]	
	end
end


def main

	# Penitencias
	penitencias = []
	penitencias[0] = "Realizar algún ejercicio físico 10 veces."
	penitencias[1] = "Recitar un poema y dedicárselo a alguien."
	penitencias[2] = "Responder tres preguntas que le harán otros jugadores."
	penitencias[3] = "Dejarse maquillar por un hombre inexperto."
	penitencias[4] = "Interpretar una canción conocida, popular y muy rítmica."
	penitencias[5] = "Gritar las frases que los demás le escriban en un papel."
	penitencias[6] = "Cargar a otro a caballito durante 20 segundos."
	penitencias[7] = "Comerse algo sin usar las manos, sólo con la boca."

	opt = 1

	system("clear")
	
	puts "********* Bienvenido al Juego de Vagos *********"
	puts

	cant_players = players()
	users = users(cant_players)
	size = board_size
	board = build_board(size)
	turnos = turnos()
	$resultados = results(cant_players)

	while (opt != 0)

		system("clear")

		jugar(board, users, turnos, $resultados)
		printResults(users, $resultados, penitencias)

		puts
		puts "0. Salir"
		puts "1. Jugar otra vez"
		puts

		opt = gets.chomp.to_i
	end
end


# build_board([13,21])

main
