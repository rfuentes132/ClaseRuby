require_relative "direccion"

class Persona

	attr_accessor :nombre, :apellido, :direcciones

	def initialize(nombre, apellido)
		@nombre = nombre
		@apellido = apellido
		@direcciones = []
	end

	def agregar_direccion(direccion)
		@direcciones.push(direccion)		
	end

	def eliminar_direccion(i_direccion)
		@direcciones.delete_at(i_direccion)
	end

	def numero_dir_habitacion
		@direcciones.select{ |x| x.es_habitacion? }.size
	end

	def dir_fiscales
		@direcciones.select{ |x| x.es_fiscal? }
	end

	def buscar_dir_por_tipo(tipo_dir, ciudad)
		@direcciones.select{ |x| (x.tipo.tipo == tipo_dir) && (x.ciudad == ciudad) }
	end
end

