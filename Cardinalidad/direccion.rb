require_relative "tipo_direccion"

class Direccion

	attr_accessor :calle, :ciudad, :pais, :tipo

	def initialize(calle, ciudad, pais)
		@calle = calle
		@ciudad = ciudad
		@pais = pais
		@tipo = TipoDireccion.new
	end

	def es_fiscal
		@tipo.tipo = "FISCAL"		
	end
	
	def es_cobro
		@tipo.tipo = "COBRO"		
	end

	def es_habitacion
		@tipo.tipo = "HABITACION"		
	end

	def es_fiscal?
		@tipo.tipo == "FISCAL"		
	end
	
	def es_cobro?
		@tipo.tipo == "COBRO"		
	end

	def es_habitacion?
		@tipo.tipo == "HABITACION"		
	end

end





