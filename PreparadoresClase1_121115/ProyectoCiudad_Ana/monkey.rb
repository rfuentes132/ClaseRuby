class Monkey

  @@legs = 2
  @@arms = 2

  # No existen accessors para variables de clase
  def arms
    @@arms    
  end

  def self.have_thumbs?
    true    
  end

  def pooping
    puts "I'm pooping"
  end

  def peeing
    puts "I'm peeing"    
  end
	
  private
  
  def evolve
    puts "I'm evolving!"    
  end

end