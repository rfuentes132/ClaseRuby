class ComplexObject

	# def initialize(tool = "hand", material = "", material2 = "")
	# 	@tool = tool
	# 	@material = material
	# 	@material2 = material2		
	# end

	def self.build(tool = "hand", material = "water", material2 = "soil")
		if material == "rock" || material == "rubber"
			return "wheel"
		elsif material == "brass" || == "iron"
			return "chassis"
		elsif material == "clay" || == "sand"
			return "chassis"
		elsif material == "plastic"
			return "tube"
		elsif material == "wheel" || == "chassis"
			return "car"
		elsif material == "brick" || == "tube"
			return "building"
		elsif material == "car" || == "building"
			return "city"
		end
	end
		
end