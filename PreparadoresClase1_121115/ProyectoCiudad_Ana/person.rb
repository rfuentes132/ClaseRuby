require_relative "monkey"
require_relative "complex_object"
require "date"

class Person < Monkey

  @@SPECIE = "homo-sapiens-sapiens"

  attr_accessor :last_name :personal_complex_object_array
  @@complex_object_array = []

  def initialize(name, last, birthday)
    @name = name
    @last_name = last
    @birthday = birthday
    @personal_complex_object_array = []
	end
	
  def name # Getter = attr_reader
    @name    
  end

  def name=(name) # Setter = attr_writer 
    @name = name
  end

  def full_name
    [@name, @last_name].join(" ")
  end

  def talk
    reasoning
  end

  def age
    self.class.age(@birthday)
    # self.class es equivalente a llamarse a si misma >> Person.age
  end

  def print_arms
    arms    
  end

  def self.age_calculate(birthday)
    age(birthday)
  end


  def building(tool = "hand", material = "water", material2 = "soil")
    complex_object_name = ComplexObject.build(tool, material, material2)    
    @@complex_object_array.push(complex_object_name)
    complex_object_name 
  end

  def personal_building(tool = "hand", material = "water", material2 = "soil")
    complex_object_name = ComplexObject.build(tool, material, material2)    
    @@complex_object_array.push(complex_object_name)
    @personal_complex_object_array.push(complex_object_name)
    complex_object_name
  end



  private
  def reasoning
    puts "I'm reasoning"
  end

  def self.age(birthday)
    unless birthday.class == Date
      birthday = Date.parse(birthday)
    end
    present = Time.now

    if (present.month - birthday.month >= 0)
      return present.year - birthday.year
    else
      return present.year - birthday.year - 1
    end
  end

end