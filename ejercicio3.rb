# Algoritmo para calcular el area de un circulo dado su diametro.

puts "Indicar diametro del circulo"

diam = gets.chomp.to_f
radio = (diam/2)
area = 3.14 * (radio)**2

puts "El area es " + area.to_s
