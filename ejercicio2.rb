# Algoritmo para calcular el area de un circulo dado el radio, sabiendo que el area es igual a 3.14 multiplicado por el radio al cuadrado.

puts "Indicar radio del circulo"

radio = gets.chomp.to_f
radio2 = radio ** 2
area = 3.14 * radio2

puts "El area es " + area.to_s
