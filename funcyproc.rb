
def minimo (x)

	if x.class != Array
		puts "El parametro debe ser un arreglo. Se introdujo #{x.class}"
		return 
	end

	min = x[0]

	for i in 0..( x.length-1 )
		if x[i] < min
			min = x[i]
		end
	end

	return min
end

def maximo (x)

	if x.class != Array
		puts "El parametro debe ser un arreglo. Se introdujo #{x.class}"
		return 
	end

	max = x[0]

	for i in 0..( x.length-1 )
		if x[i] > max
			max = x[i]
		end
	end

	return max
end

def prom (x)

	sum = 0

	for i in 0..(x.length-1)
		sum = sum + x[i]
	end

	return sum / x.length
end

def llenar_arreglo (lim)
	
	x = []

	for i in 0..lim-1
		x[i] = rand(lim**2)
	end
	return x
end

def buscar_num(x)
	puts "Introduzca numero a buscar"
	num = gets.chomp.to_i

	# Imprimir posición
	for i in 0..(x.length-1)
		if ( num == x[i] )
			puts "Posición (#{i+1})"
		end
	end
end

def mostrar_info (y, t)
	min = minimo(y)
	max = maximo(y)
	prom = prom(y)
	puts "#{t} = #{y}"
	puts "Min = #{min}"
	puts "Max = #{max}"
	puts "Prom = #{prom}"
end



a = llenar_arreglo(6)
b = llenar_arreglo(rand(10)+1)
c = llenar_arreglo(6)

mostrar_info(a, "a")
mostrar_info(b, "b")
mostrar_info(c, "Arreglo c")

