arr = Array.new(20)
pares = 0
impares = 0

arr.map! { |e| e=rand(100) }

puts arr.to_s
puts

# arr.each { |x| pares+=( x%2==0 )?1:0 }

arr.each do |e|
	if e%2==0
		pares+=1
	else
		impares+=1
	end

	if (e%2==0) && (arr.find_index(e)%2==0)
		puts "Num #{e} es par y se encuentra indice #{arr.find_index(e)} par"
	elsif (e%2!=0) && (arr.find_index(e)%2!=0)
		puts "Num #{e} es impar y se encuentra indice #{arr.find_index(e)} impar"	
	end
end

puts "Pares: #{pares}"
puts "Impares: #{impares}"

puts
puts "Indique numero a buscar en arreglo"
x = gets.chomp.to_i

if arr.include? x
	puts "Existe en #{ arr.find_index(x) }"
else
	puts "No existe"
end
