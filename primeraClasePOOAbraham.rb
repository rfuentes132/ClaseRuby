class Person

	def initialize(name, eyes) # El argumento no es necesario, ni los parentesis
		puts "creado #{name}"
		@name = name
		@eyes = eyes
	end
	

	attr_reader :name # Getter
=begin
	def name  # Otro Getter
		@name
	end
=end

	attr_writer :name # Setter
=begin
	def name= name # Otro Setter
		@name = name
	end
=end

	
	attr_accessor :eyes # Getter y Setter


	def to_s
		"Name = #{name}, Eyes = #{eyes}"
	end

	def self.has_skin # Metodo de Clase
		true		
	end

	def walk
		"#{name} walking"
	end
end


puts "#{Person.has_skin}"

jose = Person.new("jose", "negros")

puts "#{jose.to_s}"

jose.name = "Abraham Jose"
puts "#{jose.name}"

jose.eyes = "Azules"
puts "#{jose.eyes}"


class Hero < Person

	def initialize(name, power)
		super(name, "")
		@power = power
	end
	
	def walk # Overrides
		walk_padre = super # Sólo para que veas que se puede
		"#{super} power: #{@power}"		
	end
end

tony = Hero.new("Iron man", "supersuit")

puts tony.walk






