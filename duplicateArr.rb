a = (1..15).to_a

def duplicarArrX2(a)
	b = []
	a.each{ |x| b.push(x*2) }
	b
end

puts "a = #{a}"
puts

b = duplicarArrX2(a) # Duplicar todo
puts "b = #{b}"

b = a.map { |e| e=(e%2==0)?e*2:e } # Duplicar los pares
puts "b = #{b}"

b = a.map { |e| e=(e%2==0)?e:nil } # Retornar solo los pares
puts "b = #{b}"

puts "Select"
b = a.select { |e| e%2==0 } # Retornar solo los pares
puts "b = #{b}"

# Retornar solo los impares multiplos de 5, al cuadrado
b = a.select { |e| (e%2!=0)&&(e%5==0) }.map{ |e| e**2 } 
puts "b = #{b}"

puts "Inject #{(1..100).inject(:+)}"


a.map! { |e| e*5 } # Retornar solo los pares, (!) Guardar el arreglo
puts "a = #{a}"

c = ["Hola", "como", "estas"]
puts "C = #{c}"
puts c.join(" ") # Unir separado por " "


(1..10).to_a.include? 5 # Incluye ?
arr.find_index(x)