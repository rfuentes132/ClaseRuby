require_relative "cardinalidad_Direccion"

class Persona

	attr_accessor :nombre, :apellido, :direcciones

	def initialize(nombre, apellido)
		@nombre = nombre
		@apellido = apellido
		@direcciones = []
	end

	def agregar_direccion(direccion)
		@direcciones.push(direccion)
		
	end

end

