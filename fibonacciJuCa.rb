def fibonacci n
	if n > 1 # caso recursivo
		return fibonacci(n - 1) + fibonacci(n - 2)
	end

	if n == 1 # caso base
		return 1
	end

	if n == 0 #caso base
		return 0
	end
end

